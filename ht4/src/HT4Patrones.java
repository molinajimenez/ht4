
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * La clase principal del programa
 * @author Rodrigo Zea y Francisco Molina
 */
public class HT4Patrones {
     public static void main(String[] args) throws FileNotFoundException {
          //Lector de texto
  BufferedReader reader;
  //Escoge archivo
  JFileChooser menu;
  //Filtro para archivo unicamente TXT
  FileNameExtensionFilter txtOnly;
  Scanner scan = new Scanner(System.in);
        
        
      menu = new JFileChooser();
      txtOnly= new FileNameExtensionFilter("TEXT FILES", "txt", "text");
      menu.setFileFilter(txtOnly);
      
      int returnV = menu.showOpenDialog(null);
        
      if (returnV == JFileChooser.APPROVE_OPTION){ 
         reader = new BufferedReader(new FileReader(menu.getSelectedFile().getAbsoluteFile()));  
         
         
        // TODO code application logic here
        Calculadora calc = new Calculadora();
        
          System.out.println("Ingrese la opcion que quiere utilizar para la pila:");
          System.out.println("1. Utilizar Vectores");
          System.out.println("2. Utilizar ArrayList");
          System.out.println("3.1 Utilizar Lista simple");
          System.out.println("3.2 Utilizar Lista doblemente enlazada");
          System.out.println("3.3 Utilizar Lista circular");
          
          String opt = scan.nextLine();
          System.out.println(opt);
          if (!(opt.equals("1")) || (!opt.equals("2")) || (!opt.equals("3.1")) || (!opt.equals("3.2")) || (!opt.equals("3.3"))){ 
              System.out.println("Por favor seleccione una opcion valida.");
              System.exit(0);
          }
          
        try {
            
            //String instruction=calc.importData(reader);
            
            String leer = "";
            String instruction = "";
            
            while ((leer = reader.readLine()) != null){ 
                instruction = instruction + leer;
            }
            
            System.out.println("El resultado es: " + calc.calcular(instruction, opt));
            
        } catch (IOException ex) {
            System.out.println("ERROR al leer archivo.");
        }
         
      }else{ 
         
         System.out.println("Operación cancelada");
         System.exit(0);
      }
      
       
    }
}
