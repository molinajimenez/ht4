/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Molina
 */
public class Node <E> {
    protected E info;
    protected Node <E> nextElem;
    
    /**
     * constructor del nodo.
     * @param data 
     */
    public Node(E data, Node<E> nxt){
       this.info=data;
       this.nextElem = nxt;
    }
    
    /**
     * construye la cola del nodo
     * @param v
     */
    public Node(E v){ 
        this(v, null);
    }
    
    /**
     * sigue al siguiente nodo que apunta.
     * @return 
     */
    public Node<E> next(){
        return nextElem;
    }
    /**
     * define quien es el siguiente nodo
     * @param next 
     */
    public void setNext(Node<E> next){
        nextElem = next;
    }
    /**
     * retorna un valor.
     * @return 
     */
    public E value(){
        return info;
    }
    
    public void setValue(E value){ 
        info = value;
    }
    
    
    
}
