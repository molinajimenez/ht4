
/**
 * HT4 CALCULADORA
 * StackLista: Un stack de listas que permite almacenar y devolver los valores almacenados en el
 * @author Rodrigo Zea y Francisco Molina
 */
public class StackLista<E> implements Stack<E> {
    protected List<E> data;
    
    /**
     * constructor generico de la clase StackLista, devuelve una lista vacio
     * @param list
     */
    public StackLista(List list){ 
        data = list;
    }
    
    @Override
    public void push(E item) {
        data.addLast(item);
    }
    
    
    @Override
    public E pop() {
        return data.removeLast();
    }

    @Override
    public E peek() {
        return data.getLast();
    }

    @Override
    public boolean empty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return data.size();
    }
}
