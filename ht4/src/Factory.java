

/**
 * Crea una clase de tipo fabrica
 * @author Rodrigo Zea y Francisco Molina
 */
public class Factory {
    public Factory(){ 
        
    }
    
    public static Stack conseguirStack(String opt){ 
        switch (opt){ 
            case "1":
                return new StackVector();
            case "2":
                return new StackArrayList();
            case "3.1":
                return new StackLista(new SinglyLinkedList());
            case "3.2":
                return new StackLista(new DoublyLinkedList());
            case "3.3":
                return new StackLista(new CircularList());
            default:
                //predeterminado
                
                return null;
        }

    }
}
