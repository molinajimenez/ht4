
/**
 * Crea la clase abstracta de Lista, que implementa lista y se implementara en los demas tipos de lista
 * @author Rodrigo Zea
 */
public abstract class abstractList<E> implements List<E>{

    /**
     * no realiza nada
     */
    public abstractList(){ 
        
    }
    
    @Override
    /**
     * retorna true si la lista no tiene elementos
     */
    public boolean isEmpty(){ 
        return size() == 0;
    }
    
    /**
    * 
    */
    public void push(E item)
    {
        addLast(item);
    
    }
   
   /**
    *
    */
   public E pop()
   {
       
       return removeLast();
   }
  
   /**
    *
    */
   public E peek()
   {
       return getLast();
       
   }
   
   /**
    * 
    */
   public boolean empty()
   {
       return size()==0;
   }
   
   /**
    * 
    */
   public int size()
   {
       return 0;
   }
    
}
