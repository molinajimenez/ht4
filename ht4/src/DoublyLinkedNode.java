
/**
 * Crea un nodo para una lista doblemente enlazada, tiene referencias para el valor anterior y despues
 * @author Rodrigo Zea
 */
public class DoublyLinkedNode<E> {

protected E data;
protected DoublyLinkedNode<E> nextElement;
protected DoublyLinkedNode<E> previousElement;

public DoublyLinkedNode(E v, DoublyLinkedNode<E> next, DoublyLinkedNode<E> previous){
    data = v;
    nextElement = next;
    if (nextElement != null)
        nextElement.previousElement = this;
    previousElement = previous;
    if (previousElement != null)
        previousElement.nextElement = this;
}

public DoublyLinkedNode(E v)
// post: constructs a single element
{
    this(v,null,null);
}

    /**
     * sigue al siguiente nodo que apunta.
     * @return 
     */
    public DoublyLinkedNode<E> next(){
        return nextElement;
    }
    
    /**
     * sigue al nodo anterior que apunta.
     * @return 
     */
    public DoublyLinkedNode<E> previous(){
        return previousElement;
    }
    
    /**
     * define quien es el siguiente nodo
     * @param next 
     */
    public void setNext(DoublyLinkedNode<E> next){
        nextElement = next;
    }
    
     public void setValue(E value){ 
        data = value;
    }
    
       /**
     * retorna un valor.
     * @return 
     */
    public E value(){
        return data;
    }
    

}
