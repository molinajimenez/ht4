/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Molina
 */

import java.util.ArrayList;

public class StackArrayList<E> implements Stack<E> {
    protected ArrayList<E> StackList;
    
    /**
     * Constructor de StackArrayList
     */
    public StackArrayList(){
        StackList =  new ArrayList();
    }

    @Override
    public void push(E item) {
        StackList.add(item);
    }

    @Override
    public E pop() {
        return StackList.remove(StackList.size()-1);
    }

    @Override
    public E peek() {
        return StackList.remove(StackList.size()-1);
    }

    @Override
    public boolean empty() {
        return StackList.size()==0;
    }

    @Override
    public int size() {
        return StackList.size();
    }
    
}
