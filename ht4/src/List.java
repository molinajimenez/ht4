

/**
 * Clase que implementa listas en la calculadora, siendo single, double y circular.
 * @author Francisco Molina y Rodrigo Zea
 */
public interface List<E> {
    

    /**
     * Retorna el tamano de la lista, o la cantidad de valores almacenados
     * @return el tamaño de la lista
     */
    public int size();
   
    /**
     * Indica si la lista contiene elementos
     * @return booleano que indica si esta vacia la lista o no
     */
    public boolean isEmpty();
   // post: returns true iff list has no elements

    /**
     * Vacia la lista
     */
    public void clear();
   // post: empties list

    /**
     * Agrega un valor al inicio de la lista
     * @param value el valor a ser agregado al inicio
     */
    public void addFirst(E value);
   // post: value is added to beginning of list

    /**
     * Agrega un valor al final de la lista
     * @param value el valor a ser agregado al final
     */
    public void addLast(E value);
   // post: value is added to end of list

    /**
     * Retorna el primer valor de la lista
     * @return el primer valor de la lista
     */
    public E getFirst();
   // pre: list is not empty
   // post: returns first value in list

    /**
     * Retorna el ultimo valor de la lista
     * @return el ultimo valor de la lista
     */
    public E getLast();
   // pre: list is not empty
   // post: returns last value in list

    /**
     * Elimina el primer valor de la lista 
     * @return el primer valor de la lista
     */
    public E removeFirst();
   // pre: list is not empty
   // post: removes first value from list

   /**
     * Elimina el ultimo valor de la lista
     * @return el ultimo valor de la lista
     */
    public E removeLast();
   // pre: list is not empty
   // post: removes last value from list

    
}
